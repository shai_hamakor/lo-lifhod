# Don't be afraid to commit

A playground for practicing the process of contribution to open-source projects

Please follow the [contribution guide](./CONTRIBUTING.md)!

Your target as a workshop participant is to get your name added to
the [participants list](./participants.md).